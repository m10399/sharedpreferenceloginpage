package com.example.sharedpreferenceloginpage

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import com.example.sharedpreferenceloginpage.databinding.ActivityAnaEkranBinding

class AnaEkranActivity : AppCompatActivity() {
    private lateinit var binding: ActivityAnaEkranBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        binding = ActivityAnaEkranBinding.inflate(layoutInflater)
        setContentView(binding.root)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
        val sp = getSharedPreferences("GirisBilgi", Context.MODE_PRIVATE)

        val ka = sp.getString("KullaniciAdi","kullanıcı adı yok")
        val s = sp.getString("sifre","şifre yok")
        binding.textViewCikti.text = "Kullanıcı adı : $ka ve Sifre: $s"
        binding.buttonCikisYap.setOnClickListener {
            val editor = sp.edit()
            editor.remove("KullaniciAdi")
            editor.remove("sifre")
            editor.commit()
            startActivity(Intent(this@AnaEkranActivity,MainActivity::class.java))
            finish()
        }
    }
}