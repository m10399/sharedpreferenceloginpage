package com.example.sharedpreferenceloginpage

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import com.example.sharedpreferenceloginpage.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
        val sp = getSharedPreferences("GirisBilgi",Context.MODE_PRIVATE)

        val ogka = sp.getString("KullaniciAdi","kullanıcı adı yok")
        val ogs = sp.getString("sifre","şifre yok")
        if (ogka == "admin" && ogs == "123"){
            startActivity(Intent(this@MainActivity,AnaEkranActivity::class.java))
            finish()
        }
        binding.buttonGiris.setOnClickListener{

            val ka = binding.editTextTextKullaciAdi.text.toString()
            val s = binding.editTextTextKullaniciSifre.text.toString()

            if (ka == "admin" && s == "123"){
                val editor = sp.edit()
                editor.putString("KullaniciAdi",ka)
                editor.putString("sifre",s)
                editor.commit()

                startActivity(Intent(this@MainActivity,AnaEkranActivity::class.java))
                finish()
            }
            else{
                Toast.makeText(applicationContext,"Hatalı Giriş",Toast.LENGTH_SHORT).show()
            }

        }
    }
}